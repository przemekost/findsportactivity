package pl.ostrouch.findsportactivity.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexPageController {

    @GetMapping("index")
    public String getIndexPage(){
        return "Hello for test";
    }

}
