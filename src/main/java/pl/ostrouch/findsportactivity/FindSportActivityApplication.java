package pl.ostrouch.findsportactivity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindSportActivityApplication {

    public static void main(String[] args) {
        SpringApplication.run(FindSportActivityApplication.class, args);
    }
}
